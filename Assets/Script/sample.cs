﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sample : MonoBehaviour
{

    public enum eLevelState { //難易度1～3
    
        LEVEL_1,
        LEVEL_2,
        LEVEL_3,
    
    }

    eLevelState MyLevel = eLevelState.LEVEL_3;
   
    public enum eTurnState {  //先攻か後攻か
    
        TURN_BLACK,   //先攻
        TURN_WHITE,   //後攻
    
    }


    eTurnState MyTurn = eTurnState.TURN_BLACK;    //どちらかをGetPrefで所得する。デフォルトは黒
    
    public enum estoneState{    //石の状態
    
        EMPTY,
        BLACK,
        WHITE,
        CANTURN,
        
    }



    private bool BlackTurn = true;
    private bool WhiteTurn = false;

    [SerializeField]
    private GameObject Stone;   //石のプレハブ
    
    private GameObject[,] _Stones = new GameObject[Cols, Rows];   //8*8 の石を生成
  
    private Osello[,]  _StoneManager = new Osello[Cols,Rows];   //オセロの色の管理するクラス

    private estoneState _Turn = estoneState.BLACK;  //黒・白ターン（先行は黒）

    private const int Rows = 8; //横
    private const int Cols = 8; //縦

    private int x;  //タップしたx座標
    private int z;  //タップしたz座標

    //タップした石からの8方向の確認をする
    private int[] Turn_CHECK_X = new int[] { -1, -1, 0, 1, 1, 1, 0, -1 };   
    private int[] Turn_CHECK_Z = new int[] { 0, 1, 1, 1, 0, -1, -1, -1 };


    //ひっくり返す座標をリストに入れる
    public class Turnstone_c {
       
        public int c_z;
        public int c_x;

        //コンストラクタ・もし、探索した方向に敵の色があったら、このリストに座標位置を入れる
        public Turnstone_c(int z, int x) { 
      
            c_z = z;
            c_x = x;

        }
    
         
    }


    //ひっくり返すことのできる石を入れる
    private List<Turnstone_c> TurnList = new List<Turnstone_c>();

    //ひっかり返すことのできる石候補を入れる（赤色にするため）
    private List<Turnstone_c> TurnColorOnList = new List<Turnstone_c>();

    //オセロの評価値
    [SerializeField]
    private Evaluation_Num Eva_Ocello_Num_;

    //ステージマネージャへアクセス用
    [SerializeField]
    private StageManager CanStageChange;


    //前の手に戻る
    public class Undo {

        public int z_;
        public int x_;
             
        //ひっくり返したときのターンの保存
        public estoneState ENEMY_;


       //コンストラクタ
        public Undo(int z, int x,estoneState ENEMY) {
           
            z_ = z;
            x_ = x;

            ENEMY_ = ENEMY;
      
        }


    }

    //ヒックリ返した石を保存
    private List<Undo> UndoList_ = new List<Undo>();

    //選択したターンが白の場合、1ターン飛ばすカウント
    private bool Count = false;


    //選択した色を登録する
    private estoneState FirsttStone;

    //待機用コルーチン
    bool EnemyRunning = false;
    bool MyRunning = false;



    private InputManager Inputx = new InputManager();

    //ENEMYが置ける石候補のリストを生成
    List<Turnstone_c> CanTurnList = new List<Turnstone_c>();

    void Start()
    {


        for (var i = 0; i < Cols; i++)
        {   
            //石の配置
            for (var k = 0; k < Rows; k++)
            {


                _Stones[i, k] = Instantiate(Stone, new Vector3(k, 1, i), Quaternion.identity);
                _StoneManager[i, k] = _Stones[i, k].GetComponent<Osello>();
                _StoneManager[i, k].StoneState = estoneState.EMPTY;


            }

        }

        _StoneManager[4, 3].StoneState = estoneState.WHITE;
        _StoneManager[4, 4].StoneState = estoneState.BLACK;
        _StoneManager[3, 3].StoneState = estoneState.BLACK;
        _StoneManager[3, 4].StoneState = estoneState.WHITE;


        //先行か後攻かの判定
        if (MyTurn == eTurnState.TURN_BLACK) { _Turn = estoneState.BLACK; }
        if (MyTurn == eTurnState.TURN_WHITE) { _Turn = estoneState.WHITE; }


        //選択が白か黒かを保存する
        FirsttStone = _Turn;

        //もし白を選んだら最初のターンを黒にして開始する
        if (FirsttStone == estoneState.WHITE)
        {

            _Turn = ((_Turn == estoneState.BLACK) ? estoneState.WHITE : estoneState.BLACK);
            
            WhiteTurn = true;
            BlackTurn = false;
        }


        

    }

    




    void Update()
    {
       
        if (FirsttStone == _Turn)
        {
            if (BlackTurn) {


                ////置ける候補を赤く表示する
                var canupdate = true;
                TurnStart(_Turn, _StoneManager, TurnColorOnList, canupdate);

                BlackTurn = false;

            }

            Inputx.Input_Mouse_Coordinate();
            x = Inputx.Return_x();
            z = Inputx.Return_z();
            Debug.Log(x);

            //ステージ盤の範囲内でCANTURNの場合
            if (x >= 0 && x < Rows && z >= 0 && z < Cols && _StoneManager[z, x].StoneState == estoneState.CANTURN)
                {

                 //石が配置可能な場所の色付けを初期の状態にする    
                    CanStageChange.ReStart_Color();

                 //石をターンする・自分のターンのWait処理
                    StartCoroutine(My_Turn_Wait());

                    //CANTURNの状態をリセットする
                    Reset_CanTurn(_StoneManager);
                    

            }



         }
        else if(FirsttStone != _Turn)//ENEMYターン
          {

            if (WhiteTurn) {

                ////置ける候補を赤く表示する
                var canupdate = true;
                TurnStart(_Turn, _StoneManager, TurnColorOnList, canupdate);

                //石を置ける候補を敵AI生成用リストであるCanTurnListにCANTURN状態のものを入れる
                Enemy_Stone_Select(CanTurnList);


                //NULLチェック
                if (CanTurnList != null && CanTurnList.Count > 0)
                {

                    //選択したレベルに応じて、置く石の場所をかえる
                    switch (MyLevel)
                    {

                        //ランダムに石を置く
                        case eLevelState.LEVEL_1:

                            var rerult1 = LEVEL1_Return_Stone(CanTurnList);

                            x = rerult1.c_x;
                            z = rerult1.c_z;
                           

                            break;

                        //1手先に石を置く
                        case eLevelState.LEVEL_2:

                            var rerult2 = LEVEL2_Return_Stone(CanTurnList);

                            x = rerult2.c_x;
                            z = rerult2.c_z;
                           

                            break;

                        //2手先に石を置く
                        case eLevelState.LEVEL_3:

                            var rerult3 = LEVEL3_Return_Stone(CanTurnList);

                            x = rerult3.c_x;
                            z = rerult3.c_z;
                           
                            break;


                        //エラーにつきトップに戻る
                        default:

                            Debug.Log("エラー");

                            break;


                    }

                    CanTurnList.Clear();


                }
                else
                {

                    //置く石がなかったら、次のターンに行く
                    _Turn = ((_Turn == estoneState.BLACK) ? estoneState.WHITE : estoneState.BLACK);


                }
                
                WhiteTurn = false;


            }



                if (x >= 0 && x < Rows && z >= 0 && z < Cols && _StoneManager[z, x].StoneState == estoneState.CANTURN)
                {
                    //石が配置可能な場所の色付けを初期状態にする
                    CanStageChange.ReStart_Color();

                    //敵のターン処理・次ターン処理
                    StartCoroutine(Enemy_Turn_Wait());

                    foreach (var k in UndoList_)
                    { Debug.Log("Z =" + k.z_ + "X =" + k.x_ + k.ENEMY_); }

                    //ターン変更（白・黒）時に、置ける石候補のカラーをリセットする
                    Reset_CanTurn(_StoneManager);


                }



        }


    }



    //--------ここから呼び出し関数------------------------------------------------------------------------

    //ターン利用・敵ターンのwait管理
    private IEnumerator Enemy_Turn_Wait () {

        if (EnemyRunning) {

                     
            yield break;        
        
        }

        EnemyRunning = true;

        yield return new WaitForSeconds(1.0f);
        _StoneManager[z, x].StoneState = _Turn;

        yield return new WaitForSeconds(1.0f);
        var can = true;
        Turn_Cheak(_StoneManager, _Turn, x, z, TurnList, can);

        yield return new WaitForSeconds(0.5f);

        BlackTurn = true;


        _Turn = ((_Turn == estoneState.BLACK) ? estoneState.WHITE : estoneState.BLACK);


        EnemyRunning = false;
   

    }



    //ターン利用・自分ターンのwait管理
    private IEnumerator My_Turn_Wait()
    {
        if (MyRunning)
        {
            yield break;
        }

        MyRunning = true;

        //石をターンする
        _StoneManager[z, x].StoneState = _Turn;

        yield return new WaitForSeconds(0.5f);

        //ターンチェックする
        var cannot = false;
        Turn_Cheak(_StoneManager, _Turn, x, z, TurnList, cannot);

        WhiteTurn = true;


        //次のターンに移行する
        _Turn = ((_Turn == estoneState.BLACK) ? estoneState.WHITE : estoneState.BLACK);

       
        MyRunning = false;

    }

   


    //ターンチェックする
    private void Turn_Cheak(Osello[,] stones, estoneState Turn,int x,int z, List<Turnstone_c> list,bool undo) {

       //ターン開始制御
        bool CanTurn_;

       //反対の石
        estoneState enemyStone = ((Turn == estoneState.BLACK) ? estoneState.WHITE : estoneState.BLACK);


        for (var i = 0; i < Turn_CHECK_X.Length; i++)
        {
            //選択されたx・z情報をローカル変数に代入
            int stonex = x;
            int stonez = z;

            CanTurn_ = false;

            list.Clear();

            //全8方向・1方向ずつ石の状態を確認する
            while (true)
            {
                //座標を足し続ける
                stonez += Turn_CHECK_Z[i];
                stonex += Turn_CHECK_X[i];

               //座標がxが0以下で、8より大きかったらwhileからbreakする
                if (!(stonex >= 0 && stonex < Rows && stonez >= 0 && stonez < Cols))
                {

                    break;

                }

                //もし、enemystoneがあったらリストに格納する
                if (stones[stonez, stonex].StoneState == enemyStone)
                {


                    list.Add(new Turnstone_c(stonez, stonex));

                   

                }//現在のターンの色と一致する石であれば
                else if (stones[stonez, stonex].StoneState == Turn)
                {

                    
                    CanTurn_ = true;
                    break;



                }//もし、何も置かれていない石であればbreakする
                else if (stones[stonez, stonex].StoneState == estoneState.EMPTY)
                {

                    break;

                }

            }


            //ひっくり返す制御をtrueになったら
            if (CanTurn_)
            {

                foreach (var canturn in list)
                { 
                    //リストに入った石をTurnの色に変更する
                    stones[canturn.c_z, canturn.c_x].StoneState = Turn;


                    //敵AIリスト限定・元の手に戻せるようにひっくり返す座標を格納しておく
                    if (undo)
                    {

                        UndoList_.Add(new Undo(canturn.c_z, canturn.c_x, _Turn));


                    }

                }



            }




        }




    }

  

    //ひっくり返せる石の候補を出す
    private void TurnStart(estoneState Turn, Osello[,] stones, List<Turnstone_c> list,bool can) {
       
           
            bool CanTurnColor;
            
            estoneState enemyStone = ((Turn == estoneState.BLACK) ? estoneState.WHITE : estoneState.BLACK);

          
            for (var i = 0; i < Turn_CHECK_Z.Length; i++)
            {

                for (var k = 0; k < Turn_CHECK_X.Length; k++)
                {

                //石がTurnの色と一致したら
                    if (stones[i, k].StoneState == Turn)
                    {

                        for (var p = 0; p < Turn_CHECK_X.Length; p++)
                        {

                            var z1 = i;
                            var x1 = k;


                            CanTurnColor = false;

                            list.Clear();


                            while (true)
                            {
                            
                                z1 += Turn_CHECK_Z[p];
                                x1 += Turn_CHECK_X[p];

                                var z_plus = z1 + Turn_CHECK_Z[p];
                                var x_plus = x1 + Turn_CHECK_X[p];


                                if (!(x1 >= 0 && x1 < Rows && z1 >= 0 && z1 < Cols))
                                {


                                    break;

                                }

                                if (stones[z1, x1].StoneState == Turn)
                                {


                                    break;



                                }


                                if (!(x_plus >= 0 && x_plus < Rows && z_plus >= 0 && z_plus < Cols))
                                {


                                    break;

                                }



                                if (stones[z1, x1].StoneState == enemyStone && stones[z_plus, x_plus].StoneState == estoneState.EMPTY)
                                {

                                    if (can) {

                                        CanStageChange.Red_Change_Color(z_plus, x_plus);

                                    }
                               
                                    list.Add(new Turnstone_c(z_plus, x_plus));
                               
                                    CanTurnColor = true;

                                    break;

                                }
                                else if (stones[z1, x1].StoneState == enemyStone)
                                {

                                    continue;


                                }
                                else
                                {

                                    break;
                                }


                            }


                            if (CanTurnColor)
                            {

                                foreach (var canturn in list)
                                {

                                    stones[canturn.c_z, canturn.c_x].StoneState = estoneState.CANTURN;

                                }


                            }



                        }



                }
              

            }




        }
       



    }

    //ターン変更（白・黒）時に、置ける石候補のカラーをリセットする
    private void Reset_CanTurn(Osello[,] stones) {

        for (var i = 0; i < Cols; i++) {


            for (var k = 0; k < Rows; k++) {


                if (stones[i, k].StoneState == estoneState.CANTURN) {


                    stones[i, k].StoneState = estoneState.EMPTY;


                }               
                
            
            
            
            }
        
        
        
        }
     
    
    
    }


    //石を置ける候補を敵AI生成用リストであるCanTurnListにCANTURN状態のものを入れる
    private void Enemy_Stone_Select(List<Turnstone_c> TurnList) {


        for (var i = 0; i < Cols; i++)
        {

            for (var k = 0; k < Rows; k++)
            {

                
                if (_StoneManager[i, k].StoneState == estoneState.CANTURN)
                {

                    TurnList.Add(new Turnstone_c(i, k));


                }


            }


        }

        

    }


    //難易度1・簡単
    //評価値関係なく、ランダムで配置する
    private Turnstone_c LEVEL1_Return_Stone(List<Turnstone_c> canputstone) {

        var  rerult = canputstone[Random.Range(0, canputstone.Count)];
    
        return rerult;

    }


    //難易度2・普通・一手先を読む
    private Turnstone_c LEVEL2_Return_Stone(List<Turnstone_c> canputstone) {

       
        Turnstone_c rerult = canputstone[0];
        var score = Eva_Ocello_Num_.Eva_Nums2_[rerult.c_z, rerult.c_x];


        foreach (var i in canputstone)
        {
            

            if (score <= Eva_Ocello_Num_.Eva_Nums2_[i.c_z, i.c_x])
            {

                score = Eva_Ocello_Num_.Eva_Nums2_[i.c_z, i.c_x];
                rerult = i;
               

            }
         
     

        }
       
        return rerult;

       

    }


    //難易度3・最難関・2手先を読む
    private Turnstone_c LEVEL3_Return_Stone(List<Turnstone_c> canputstone) {

        //リターンする変数にcanputstone[0]を入れる
        Turnstone_c rerult = canputstone[0];
        var resultScore = 0;
       

        foreach (var i in canputstone)
        {
            //undoリストをクリア
            UndoList_.Clear();

            //リストのCANTURNをひとつづつ、ターンする
            _StoneManager[i.c_z, i.c_x].StoneState = _Turn;

           //ターンチェックをする
            var can = true;
            Turn_Cheak(_StoneManager, _Turn, i.c_x, i.c_z, TurnList, can);

        
            //スコアを採点する（第一手目・プラスしていく）
            var score = Eva_Ocello_Num_.Return_Eve_Num(_StoneManager, _Turn);
            var testscore = Eva_Ocello_Num_.Eva_Nums2_[i.c_z, i.c_x];
            score += testscore;
            Debug.Log("スコア1　　　" + score);

            //2手先を読む（毎ターンで最も高得点の評価値を選びscoreにマイナスする）
            foreach (var k in UndoList_) { 
            
                var stackscore = (-1 * Eva_Ocello_Num_.Eva_Nums2_[k.z_, k.x_]);

                 score += stackscore;
                 Debug.Log("スコア2　　"+score);
                 Debug.Log("スタック　　" + stackscore);

                //現在のスコアがrerultscoreよりも大きかったら
                if (resultScore <= score) {

                    //スコアを更新し
                    resultScore = score;
                    
                    //リターンするクラスを更新する
                    rerult = i;
                
                
                }

              


            }

            //i番目の処理が終わったら、一手先に戻す
            var u = new Undo(i.c_z, i.c_x, _Turn);
            Backstone(u);

            Debug.Log("リザルト"+resultScore);

        }

      
        return rerult;



    }

  

    //一手先に戻す
    void Backstone(Undo undo) {

       //反対のターン情報を変数に代入する
        estoneState enemyStone = ((_Turn == estoneState.BLACK) ? estoneState.WHITE : estoneState.BLACK);

        //ひっくり返す前のターンに戻す
        foreach (var i in UndoList_) {

            _StoneManager[i.z_, i.x_].StoneState = enemyStone;
                       
        }

        //石を置いてる状態から石を置ける状態に戻す
        _StoneManager[undo.z_, undo.x_].StoneState = estoneState.CANTURN;
        CanStageChange.ReStart_Color();
    

    }



}
