﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{

    public enum eBord_color
    { //ボードの色管理

        B_BLACK,
        B_WHITE,
        B_RED,

    }

    [SerializeField]
    StageCubeColor _stoneboxPrefab;

    [SerializeField]
    StageCubeColor[,] _stoneboxs = new StageCubeColor[_width, _height];

    private eBord_color[,] _bordColor = new eBord_color[_width, _height];
    
    [SerializeField]
    private GameObject StagePrefab;
    
    private GameObject[,] PlanePrefab = new GameObject[_height,_width];

    private const int _width = 8;
    private const int _height = 8;

    eBord_color test;

    void Start()
    {

        Start_Color2();

    }


    void Update()
    {

        

    }


    public void Red_Change_Color(int z, int x) {


       // Debug.Log(z);
        
      _stoneboxs[z, x].GetComponent<StageCubeColor>()._stagecolor_now = eBord_color.B_RED;
        


    }




  


    public void Start_Color2()
    {

        for (var i = 0; i < _height; i++)
        {


            for (var k = 0; k < _width; k++)
            {


                if (i % 2 == 0)
                {


                    if (k % 2 == 0)
                    {


                        var stone = Instantiate(this._stoneboxPrefab, new Vector3(k, 0, i), Quaternion.identity);
                        _stoneboxs[i, k] = stone;

                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_BLACK;



                    }
                    else
                    {


                        var stone = Instantiate(this._stoneboxPrefab, new Vector3(k, 0, i), Quaternion.identity);
                        _stoneboxs[i, k] = stone;

                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_WHITE;


                    }



                }
                else
                {

                    if (k % 2 == 0)
                    {


                        var stone = Instantiate(this._stoneboxPrefab, new Vector3(k, 0, i), Quaternion.identity);
                        _stoneboxs[i, k] = stone;

                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_WHITE;


                    }
                    else
                    {

                        var stone = Instantiate(this._stoneboxPrefab, new Vector3(k, 0, i), Quaternion.identity);
                        _stoneboxs[i, k] = stone;

                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_BLACK;



                    }


                }






            }




        }




    }




    public void ReStart_Color() {



        for (var i = 0; i < _height; i++)
        {


            for (var k = 0; k < _width; k++)
            {


                if (i % 2 == 0)
                {


                    if (k % 2 == 0)
                    {


                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_BLACK;


                    }
                    else
                    {


                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_WHITE;


                    }



                }
                else
                {

                    if (k % 2 == 0)
                    {

                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_WHITE;





                    }
                    else
                    {
                        var stonecolor = _stoneboxs[i, k].GetComponent<StageCubeColor>();
                        stonecolor._stagecolor_now = eBord_color.B_BLACK;



                    }


                }






            }




        }







    }






}


 





