﻿using UnityEngine;

public class Evaluation_Num : MonoBehaviour
{

    private const int cols = 8;
    private const int rows = 8;

    public int[,] Eva_Nums2_ = new int[cols, rows]
        
        {   //評価値
        
          {120, -20,  20,   5,   5,  20, -20, 120},
          {-20, -40,  -5,  -5,  -5,  -5, -40, -20},
          { 20,  -5,  15,   3,   3,  15,  -5,  20},
          {  5,  -5,   3,   3,   3,   3,  -5,   5},
          {  5,  -5,   3,   3,   3,   3,  -5,   5},
          { 20,  -5,  15,   3,   3,  15,  -5,  20},
          {-20, -40,  -5,  -5,  -5,  -5, -40, -20},
          {120, -20,  20,   5,   5,  20, -20, 120},
             
        
        };


  
    void Start()
    {


    }

  
    void Update()
    {
        
    }


    public int Return_Eve_Num(Osello[,] stone,sample.estoneState turn) {

        
        var score = 0;


        if (turn == sample.estoneState.BLACK)
        {

            for (var i = 0; i < cols; i++)
            {



                for (var k = 0; k < rows; k++)
                {


                    if (stone[i, k].StoneState == sample.estoneState.BLACK) {


                        score += Eva_Nums2_[i, k];
                    
                    }



                }



            }



            return score;



        }
        else if (turn == sample.estoneState.WHITE) {



            for (var i = 0; i < cols; i++)
            {



                for (var k = 0; k < rows; k++)
                {


                    if (stone[i, k].StoneState == sample.estoneState.WHITE)
                    {

                        score += Eva_Nums2_[i, k];


                    }




                }



            }








        }



        return score;
    
    }






}
