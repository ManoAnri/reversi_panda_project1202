﻿using UnityEngine;

public class StageCubeColor : MonoBehaviour
{

    public StageManager.eBord_color _stagecolor_now;
    public bool _CanTurnColor;

    [SerializeField]
    private MeshRenderer _Turn_box_redcolor;


    private MeshRenderer _stage_color;

    // Start is called before the first frame update
    void Start()
    {

        _stage_color = this.GetComponent<MeshRenderer>();     

    }

    // Update is called once per frame
    void Update()
    {

       CheckBoxColor();
       
    }



    private void CheckBoxColor() {


        switch (_stagecolor_now) {

            case StageManager.eBord_color.B_BLACK:
                _stage_color.material.color = Color.green;
                break;

            case StageManager.eBord_color.B_WHITE:
                _stage_color.material.color = Color.yellow;
                break;
            case StageManager.eBord_color.B_RED:
                _stage_color.material.color = Color.red;
                break;

        }
    
        

    
    }


  






}
