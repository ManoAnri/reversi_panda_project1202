﻿using UnityEngine;

public class Osello : MonoBehaviour
{
     
  public sample.estoneState StoneState = sample.estoneState.EMPTY; 

    [SerializeField]
    private MeshRenderer Stote_ = null;
    
    private MeshRenderer k;


    // Start is called before the first frame update
    void Start()
    {

        k = Stote_.GetComponent<MeshRenderer>();
        
    }

    // Update is called once per frame
    void Update()
    {

        ChangeStone();

    }



    public void ChangeStone()
    {
        
        switch (StoneState) {

            case sample.estoneState.EMPTY:
                k.enabled = false;
                break;

            case sample.estoneState.CANTURN:
                k.enabled = false;
                break;

            case sample.estoneState.BLACK:
                k.enabled = true;
                k.material.color = Color.black;
                break;

            case sample.estoneState.WHITE:
                k.enabled = true;
                k.material.color = Color.white;
                break;

           

        }

    }

}
